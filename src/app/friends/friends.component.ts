import {
  Component,
  Input,
} from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { MdDialog, MdDialogRef } from '@angular/material';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

import { UserBaseInfo, Card } from '../_models/index'
import { VKService } from '../_services/index';

@Component({
  selector: 'friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css'],
  animations: [
    trigger('cardState', [
      state('inactive', style({
        backgroundColor: '#fff',
        transform: 'scale(1)'
      })),
      state('active', style({
        backgroundColor: 'rgba(173, 237, 255, 0.15)',
        transform: 'scale(1.01)'
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out'))
    ])
  ]
})

export class FriendsComponent {
  @Input() 
  public card: Card;

  @Input() 
  public users;
  
  constructor(
    private vkService: VKService,
    public dialog: MdDialog
  ) { }

  async click(user: UserBaseInfo) {
    this.openDialog(user)
  }

  openDialog(user: UserBaseInfo) {
    window.open("https://m.vk.com/im?sel="+user.user_id, "_blank", "width=500,height=400,top=0")
  }
}