import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';

import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

@NgModule({
  declarations: [
    MessageDialogComponent,
  ],
  entryComponents: [ MessageDialogComponent ],
  imports: [
    MaterialModule,
  ],
})
export class FriendsModule { }