import {
  Component,
  Input,
} from '@angular/core';


@Component({
  selector: 'count-friends',
  templateUrl: './count-friends.component.html',
  styleUrls: ['./count-friends.component.css']
})
export class CountFriendsComponent  {
  @Input()
  public count: number;

  ngOnInit() {
    console.log(this);
  }
}
