import {
  Component,
  ComponentFactoryResolver, ViewChild, ViewContainerRef,
  OnInit,
} from '@angular/core';

import { UserBaseInfo, Card } from './_models'
import { NewsComponent } from './news/news.component'
import { FriendsComponent } from './friends/friends.component'
import { CountFriendsComponent } from './count-friends/count-friends.component'
import { VKService } from './_services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    private text: string = "ФБК раскрыл картельный сговор 5 компаний на торгах МВД на общую сумму почти в 5 млрд рублей. ФБК раскрыл картельный сговор 5 компаний на торгах МВД на общую сумму почти в 5 млрд рублей. В 2015-2017 годах машины для полиции закупались по завышенной стоимости, а документацию подписывал замначальника управления организации материально-технического обеспечения МВД Сергей Горелик, который в 2013, по сообщениям СМИ, уже фигурировал в махинациях на госзакупках МВД."
    public cards: Card[] = [ 
        new Card("Любовь и картель", this.text, "https://goo.gl/1keBTy"),
        new Card("Любовь и картель", this.text, "https://goo.gl/1keBTy"),
        new Card("Любовь и картель", this.text, "https://goo.gl/1keBTy"),
    ]

    private friends$: Promise<UserBaseInfo[]>;
    private filteredFriends$: Promise<UserBaseInfo[]>;
    private friendsCount$: Promise<number>;
    private selectedNews: Card;
    private isWritePersonalSelected: boolean;

    @ViewChild('dynamicInsertCountFriends', { read: ViewContainerRef }) dynamicInsertCountFriends: ViewContainerRef;
    @ViewChild('dynamicInsertNews', { read: ViewContainerRef }) dynamicInsertNews: ViewContainerRef;
    @ViewChild('dynamicInsert', { read: ViewContainerRef }) dynamicInsert: ViewContainerRef;
    constructor(
        private vkService: VKService, 
        private componentFactoryResolver: ComponentFactoryResolver) {
    }

    async ngOnInit(): Promise<void> {
      console.log("start app");
      await this.vkService.auth();
      this.friends$ = this.vkService.getFriends();
      const friends = await this.friends$;
      await this.vkService.getSubscriptions(friends);
    }

    async postOnWall() {
        this.isWritePersonalSelected = false; 
        await this.vkService.postOnWall(this.selectedNews);
    }


    public setSetlectedNews(card: Card): void {
        this.selectedNews = card;
        this.filterFriends();
    }

    public filterFriends(): void {
      // TODO implement server 
        this.filteredFriends$ = this.friends$.then((list) => {
            let currentIndex = list.length, temporaryValue, randomIndex;
            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                temporaryValue = list[currentIndex];
                list[currentIndex] = list[randomIndex];
                list[randomIndex] = temporaryValue;
            }
            const random2 = Math.floor(Math.random() * (list.length -1)) + 1
            const filteredLength = random2 == 0 ? 1 : random2;
            return list.slice(0, filteredLength - 1); 
        });
        this.friendsCount$ = this.filteredFriends$.then((list) => {return list.length});
    }
}
