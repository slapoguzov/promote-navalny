import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { Card } from '../_models/index'

@Component({
  selector: 'news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  animations: [
    trigger('cardState', [
      state('inactive', style({
        backgroundColor: '#fff',
        transform: 'scale(1)'
      })),
      state('active',   style({
        backgroundColor: 'rgba(173, 237, 255, 0.15)',
        transform: 'scale(1.01)'
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out'))
    ])
  ]
})

export class NewsComponent  {
  @Input()
  public cards: Card[];

  @Output()
  public setSelected = new EventEmitter<Card>();

  public selectCard(card: Card): void {
    for(let cr of this.cards) {
      cr.state = "inactive"
    }
    card.selectCard();
    this.setSelected.emit(card);
  }
}
