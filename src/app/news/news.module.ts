import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';

import { NewsComponent } from './news.component';

@NgModule({
  declarations: [
    NewsComponent,
  ],
  imports: [
    MaterialModule,
  ],
})
export class NewsModule { }