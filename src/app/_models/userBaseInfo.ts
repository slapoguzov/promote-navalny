import { Subscriptions } from './index'


export class UserBaseInfo {
    can_write_private_message: boolean;
    first_name: string;
    last_name: string;
    nickname: string;
    online: boolean;
    photo_100: string;
    uid: number;
    user_id: number;
    subscriptions: Subscriptions;
    friends: UserBaseInfo[];
}