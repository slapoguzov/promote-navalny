export class Card {
    title: String;
    text: String;
    photo: String;
    state: String;

    constructor(title: String,
        text: String,
        photo: String) {
            this.title = title;
            this.text = text;
            this.photo = photo;
            this.state = "inactive"
    }
    selectCard() {
      if(this.state == "inactive") {
          this.state = "active"
      } else {
        this.state = "inactive"
      }
    }
}