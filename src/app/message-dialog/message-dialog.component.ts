import {
  Component,
  Input,
} from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

import { UserBaseInfo, Card } from '../_models/index'
import { VKService } from '../_services/index';

@Component({
  selector: 'message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.css']
})
export class MessageDialogComponent {
  @Input('text') text;
  constructor(public dialogRef: MdDialogRef<MessageDialogComponent>) { }
}
