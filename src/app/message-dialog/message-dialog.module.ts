import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';

@NgModule({
  declarations: [

  ],
  imports: [
    MaterialModule,
  ],
})
export class MessageDialogModule { }