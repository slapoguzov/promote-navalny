import { Injectable } from '@angular/core';
import { UserBaseInfo, Subscriptions } from '../_models/index';
import { serialize } from "serializer.ts/Serializer";
import { deserialize } from "serializer.ts/Serializer";

import { Card } from "../_models/index";

declare var VK: any;

@Injectable()
export class VKService {
    constructor() {
        VK.init({
            apiId: 6146379
        })
    }

    async auth(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            VK.Auth.getLoginStatus((data) => {
                console.log("auth status: ", data['status'])
                if (data['status'] != 'connected') {
                    VK.Auth.login((data) => {
                        resolve(true)
                    }, 1 + 2 + 4096 + 8192)
                } else {
                    resolve(false)
                }
            })
        })
    }

    async sendMessage(userId: number, card: Card): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            VK.Api.call('messages.send',
                { user_id: userId, message: card.text, random_id: Math.floor(Math.random() * 9999999999999) + 1 },
                function (r) {
                    console.log(r)
                    resolve(r.response)
                });
        })
    }

    async postOnWall(card: Card): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            VK.Api.call('wall.post', { message: card.text }, function (r) {
                resolve(r.response)
            });
        })
    }

    async getFriends(userId: number = 0): Promise<UserBaseInfo[]> {
        return new Promise<UserBaseInfo[]>((resolve, reject) => {
            VK.Api.call('friends.get', { user_id: userId, fields: "nickname, photo_100, online, can_write_private_message" }, function (r) {
                resolve(deserialize<UserBaseInfo[]>(UserBaseInfo, r.response))
            });
        })
    }

    async getSubscriptions(users: UserBaseInfo[]): Promise<UserBaseInfo[]> {
        if (users.length > 12) {
            var enrichUsers = new Array()
            for (var _i = 0; _i < users.length; _i = _i + 12) {
                let newsUsers = users.slice(_i, _i + 12)
                let result = await this.getSubscriptions(newsUsers);
                enrichUsers.push.apply(enrichUsers, result)
            }
            return new Promise<UserBaseInfo[]>((resolve, reject) => { resolve(enrichUsers) })
        } else {
            var code = "return [";
            for (var user of users) {
                let userId = user.user_id
                code +=
                    '[' + userId +
                    ',API.friends.get({ user_id: ' + userId + ', fields: "nickname, photo_100, online, can_write_private_message" })' +
                    ',API.users.getSubscriptions({ user_id: ' + userId + ', fields: "id", extended: "0" })],'
            }
            code += "];"
            return new Promise<UserBaseInfo[]>((resolve, reject) => {
                VK.Api.call('execute', { code: code }, (r) => {
                    r.response.forEach((resp) => {
                        let respUserId = resp[0];
                        let respFrineds = resp[1];
                        let respSubscrtion = resp[2];
                        if (respSubscrtion !== false) {
                            let subscriptions = new Subscriptions
                            subscriptions.groups = respSubscrtion['groups']['items']
                            subscriptions.users = respSubscrtion['users']['items']
                            let user = users.filter(user => user.user_id === respUserId)[0]
                            user.subscriptions = subscriptions;
                            user.friends = deserialize<UserBaseInfo[]>(UserBaseInfo, respFrineds)
                        }
                    });
                    resolve(users)
                })
            })
        }
    }
}