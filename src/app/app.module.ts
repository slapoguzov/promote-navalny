import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { VKService } from './_services/index';

import { AppComponent } from './app.component';
import { NewsComponent } from './news/news.component';
import { FriendsComponent } from './friends/friends.component';
import { CountFriendsComponent } from './count-friends/count-friends.component';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';
import { SendMessageIframeComponent } from './send-message-iframe/send-message-iframe.component';

@NgModule({
  declarations: [
    AppComponent,
    NewsComponent,
    FriendsComponent,
    CountFriendsComponent,
    MessageDialogComponent,
    SendMessageIframeComponent
  ],
  entryComponents: [
    FriendsComponent, 
    NewsComponent,
   // CountFriendsComponent,
    MessageDialogComponent,
    SendMessageIframeComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
  ],
  providers: [
    VKService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }