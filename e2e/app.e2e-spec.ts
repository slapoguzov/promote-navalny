import { PromoteNavalnyPage } from './app.po';

describe('promote-navalny App', () => {
  let page: PromoteNavalnyPage;

  beforeEach(() => {
    page = new PromoteNavalnyPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
